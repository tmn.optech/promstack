
## Требования

- Helm 3+

## Установка

```console
helm -n monitoring upgrade --install grafana-operator -f values.yml .
```
### Схема оператора
```
+--------------------+
|                    |      +---------------+
|  Grafana Operator  |      |               |
|                    |      |     RBAC      |
|    Deployment      |      |   Privileges  |
|                    |      |               |
+-------+------------+      +-------+-------+
        ^                           |
        |   +-----------------+     |
        +---+ Service Account +<----+
            +-----------------+
```

The operator will extend the Kubernetes API with the following objects: *Grafana*, *GrafanaDashboards* and *GrafanaDataSources*. From that moment, the user will be able to deploy objects of these kinds and the previously deployed Operator will take care of deploying all the required Deployments, ConfigMaps and Services for running a Grafana instance. Its lifecycle is managed using *kubectl* on the Grafana, GrafanaDashboards and GrafanaDataSource objects. The following figure shows the deployed objects after
 deploying a *Grafana* object using *kubectl*:

```
+--------------------+
|                    |      +---------------+
|  Grafana Operator  |      |               |
|                    |      |     RBAC      |
|    Deployment      |      |   Privileges  |
|                    |      |               |
+--+----+------------+      +-------+-------+
   |    ^                           |
   |    |   +-----------------+     |
   |    +---+ Service Account +<----+
   |        +-----------------+
   |
   |
   |
   |
   |                                                   Grafana
   |                     +---------------------------------------------------------------------------+
   |                     |                                                                           |
   |                     |                          +--------------+             +-----+             |
   |                     |                          |              |             |     |             |
   +-------------------->+       Service & Ingress  |    Grafana   +<------------+ PVC |             |
                         |      <-------------------+              |             |     |             |
                         |                          |  Deployment  |             +-----+             |
                         |                          |              |                                 |
                         |                          +-----------+--+                                 |
                         |                                      ^                +------------+      |
                         |                                      |                |            |      |
                         |                                      +----------------+ Configmaps |      |
                         |                                                       |   Secrets  |      |
                         |                                                       |            |      |
                         |                                                       +------------+      |
                         |                                                                           |
                         +---------------------------------------------------------------------------+

```

### Global parameters

| Name                      | Description                                     | Value |
| ------------------------- | ----------------------------------------------- | ----- |
| `global.imageRegistry`    | Global Docker image registry                    | `nil` |
| `global.imagePullSecrets` | Global Docker registry secret names as an array | `[]`  |
| `global.storageClass`     | Global StorageClass for Persistent Volume(s)    | `nil` |


### Common parameters

| Name               | Description                                                                             | Value           |
| ------------------ | --------------------------------------------------------------------------------------- | --------------- |
| `kubeVersion`      | Force target Kubernetes version (using Helm capabilities if not set)                    | `nil`           |
| `extraDeploy`      | Array of extra objects to deploy with the release                                       | `[]`            |
| `nameOverride`     | String to partially override grafana.fullname template (will maintain the release name) | `nil`           |
| `fullnameOverride` | String to fully override grafana.fullname template                                      | `nil`           |
| `clusterDomain`    | Default Kubernetes cluster domain                                                       | `cluster.local` |


### Grafana parameters

| Name                               | Description                                                                       | Value                |
| ---------------------------------- | --------------------------------------------------------------------------------- | -------------------- |
| `image.registry`                   | Grafana image registry                                                            | `docker.io`          |
| `image.repository`                 | Grafana image repository                                                          | `bitnami/grafana`    |
| `image.tag`                        | Grafana image tag (immutable tags are recommended)                                | `8.0.4-debian-10-r0` |
| `image.pullPolicy`                 | Grafana image pull policy                                                         | `IfNotPresent`       |
| `image.pullSecrets`                | Grafana image pull secrets                                                        | `[]`                 |
| `hostAliases`                      | Add deployment host aliases                                                       | `[]`                 |
| `admin.user`                       | Grafana admin username                                                            | `admin`              |
| `admin.password`                   | Admin password. If a password is not provided a random password will be generated | `nil`                |
| `admin.existingSecret`             | Name of the existing secret containing admin password                             | `nil`                |
| `admin.existingSecretPasswordKey`  | Password key on the existing secret                                               | `password`           |
| `smtp.enabled`                     | Enable SMTP configuration                                                         | `false`              |
| `smtp.user`                        | SMTP user                                                                         | `user`               |
| `smtp.password`                    | SMTP password                                                                     | `password`           |
| `smtp.host`                        | Custom host for the smtp server                                                   | `nil`                |
| `smtp.existingSecret`              | Name of existing secret containing SMTP credentials (user and password)           | `nil`                |
| `smtp.existingSecretUserKey`       | User key on the existing secret                                                   | `user`               |
| `smtp.existingSecretPasswordKey`   | Password key on the existing secret                                               | `password`           |
| `plugins`                          | Grafana plugins to be installed in deployment time separated by commas            | `nil`                |
| `ldap.enabled`                     | Enable LDAP for Grafana                                                           | `false`              |
| `ldap.allowSignUp`                 | Allows LDAP sign up for Grafana                                                   | `false`              |
| `ldap.configMapName`               | Name of the ConfigMap with the LDAP configuration file for Grafana                | `nil`                |
| `extraEnvVars`                     | Array containing extra env vars to configure Grafana                              | `{}`                 |
| `extraConfigmaps`                  | Array to mount extra ConfigMaps to configure Grafana                              | `{}`                 |
| `config.useGrafanaIniFile`         | Allows to load a `grafana.ini` file                                               | `false`              |
| `config.grafanaIniConfigMap`       | Name of the ConfigMap containing the `grafana.ini` file                           | `nil`                |
| `config.grafanaIniSecret`          | Name of the Secret containing the `grafana.ini` file                              | `nil`                |
| `dashboardsProvider.enabled`       | Enable the use of a Grafana dashboard provider                                    | `false`              |
| `dashboardsProvider.configMapName` | Name of a ConfigMap containing a custom dashboard provider                        | `nil`                |
| `dashboardsConfigMaps`             | Array with the names of a series of ConfigMaps containing dashboards files        | `[]`                 |
| `datasources.secretName`           | Secret name containing custom datasource files                                    | `nil`                |


### Deployment parameters

| Name                                 | Description                                                                               | Value           |
| ------------------------------------ | ----------------------------------------------------------------------------------------- | --------------- |
| `replicaCount`                       | Number of Grafana nodes                                                                   | `1`             |
| `updateStrategy.type`                | Set up update strategy for Grafana installation.                                          | `RollingUpdate` |
| `schedulerName`                      | Alternative scheduler                                                                     | `nil`           |
| `priorityClassName`                  | Priority class name                                                                       | `""`            |
| `podLabels`                          | Extra labels for Grafana pods                                                             | `{}`            |
| `podAnnotations`                     | Grafana Pod annotations                                                                   | `{}`            |
| `podAffinityPreset`                  | Pod affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`       | `""`            |
| `podAntiAffinityPreset`              | Pod anti-affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`  | `soft`          |
| `nodeAffinityPreset.type`            | Node affinity preset type. Ignored if `affinity` is set. Allowed values: `soft` or `hard` | `""`            |
| `nodeAffinityPreset.key`             | Node label key to match Ignored if `affinity` is set.                                     | `""`            |
| `nodeAffinityPreset.values`          | Node label values to match. Ignored if `affinity` is set.                                 | `[]`            |
| `affinity`                           | Affinity for pod assignment                                                               | `{}`            |
| `nodeSelector`                       | Node labels for pod assignment                                                            | `{}`            |
| `tolerations`                        | Tolerations for pod assignment                                                            | `[]`            |
| `securityContext.enabled`            | Enable securityContext on for Grafana deployment                                          | `true`          |
| `securityContext.fsGroup`            | Group to configure permissions for volumes                                                | `1001`          |
| `securityContext.runAsUser`          | User for the security context                                                             | `1001`          |
| `securityContext.runAsNonRoot`       | Run containers as non-root users                                                          | `true`          |
| `resources.limits`                   | The resources limits for Grafana containers                                               | `{}`            |
| `resources.requests`                 | The requested resources for Grafana containers                                            | `{}`            |
| `livenessProbe.enabled`              | Enable livenessProbe                                                                      | `true`          |
| `livenessProbe.initialDelaySeconds`  | Initial delay seconds for livenessProbe                                                   | `120`           |
| `livenessProbe.periodSeconds`        | Period seconds for livenessProbe                                                          | `10`            |
| `livenessProbe.timeoutSeconds`       | Timeout seconds for livenessProbe                                                         | `5`             |
| `livenessProbe.failureThreshold`     | Failure threshold for livenessProbe                                                       | `6`             |
| `livenessProbe.successThreshold`     | Success threshold for livenessProbe                                                       | `1`             |
| `readinessProbe.enabled`             | Enable readinessProbe                                                                     | `true`          |
| `readinessProbe.initialDelaySeconds` | Initial delay seconds for readinessProbe                                                  | `30`            |
| `readinessProbe.periodSeconds`       | Period seconds for readinessProbe                                                         | `10`            |
| `readinessProbe.timeoutSeconds`      | Timeout seconds for readinessProbe                                                        | `5`             |
| `readinessProbe.failureThreshold`    | Failure threshold for readinessProbe                                                      | `6`             |
| `readinessProbe.successThreshold`    | Success threshold for readinessProbe                                                      | `1`             |
| `sidecars`                           | Attach additional sidecar containers to the Grafana pod                                   | `{}`            |
| `extraVolumes`                       | Additional volumes for the Grafana pod                                                    | `[]`            |
| `extraVolumeMounts`                  | Additional volume mounts for the Grafana container                                        | `[]`            |


### Persistence parameters

| Name                        | Description                                                                                               | Value           |
| --------------------------- | --------------------------------------------------------------------------------------------------------- | --------------- |
| `persistence.enabled`       | Enable persistence                                                                                        | `true`          |
| `persistence.accessMode`    | Access mode to the PV                                                                                     | `ReadWriteOnce` |
| `persistence.storageClass`  | Storage class to use with the PVC                                                                         | `nil`           |
| `persistence.existingClaim` | If you want to reuse an existing claim, you can pass the name of the PVC using the existingClaim variable | `nil`           |
| `persistence.size`          | Size for the PV                                                                                           | `10Gi`          |


### RBAC parameters

| Name                         | Description                                                                                                           | Value  |
| ---------------------------- | --------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create`      | Specifies whether a ServiceAccount should be created                                                                  | `true` |
| `serviceAccount.name`        | The name of the ServiceAccount to use. If not set and create is true, a name is generated using the fullname template | `nil`  |
| `serviceAccount.annotations` | Annotations to add to the ServiceAccount Metadata                                                                     | `{}`   |


### Traffic exposure parameters

| Name                               | Description                                                                                   | Value                    |
| ---------------------------------- | --------------------------------------------------------------------------------------------- | ------------------------ |
| `service.type`                     | Kubernetes Service type                                                                       | `ClusterIP`              |
| `service.port`                     | Grafana service port                                                                          | `3000`                   |
| `service.nodePort`                 | Specify the nodePort value for the LoadBalancer and NodePort service types                    | `nil`                    |
| `service.loadBalancerIP`           | loadBalancerIP if Grafana service type is `LoadBalancer` (optional, cloud specific)           | `nil`                    |
| `service.loadBalancerSourceRanges` | loadBalancerSourceRanges if Grafana service type is `LoadBalancer` (optional, cloud specific) | `[]`                     |
| `service.annotations`              | Provide any additional annotations which may be required.                                     | `{}`                     |
| `ingress.enabled`                  | Set to true to enable ingress record generation                                               | `false`                  |
| `ingress.certManager`              | Set this to true in order to add the corresponding annotations for cert-manager               | `false`                  |
| `ingress.pathType`                 | Ingress Path type                                                                             | `ImplementationSpecific` |
| `ingress.apiVersion`               | Override API Version (automatically detected if not set)                                      | `nil`                    |
| `ingress.hostname`                 | When the ingress is enabled, a host pointing to this will be created                          | `grafana.local`          |
| `ingress.path`                     | Default path for the ingress resource                                                         | `ImplementationSpecific` |
| `ingress.annotations`              | Ingress annotations                                                                           | `{}`                     |
| `ingress.tls`                      | Enable TLS configuration for the hostname defined at ingress.hostname parameter               | `false`                  |
| `ingress.extraHosts`               | The list of additional hostnames to be covered with this ingress record.                      | `[]`                     |
| `ingress.extraPaths`               | Any additional arbitrary paths that may need to be added to the ingress under the main host.  | `[]`                     |
| `ingress.extraTls`                 | The tls configuration for additional hostnames to be covered with this ingress record.        | `[]`                     |
| `ingress.secrets`                  | If you're providing your own certificates, please use this to add the certificates as secrets | `[]`                     |
| `ingress.secrets`                  | It is also possible to create and manage the certificates outside of this helm chart          | `[]`                     |


### Metrics parameters

| Name                                   | Description                                                                                            | Value   |
| -------------------------------------- | ------------------------------------------------------------------------------------------------------ | ------- |
| `metrics.enabled`                      | Enable the export of Prometheus metrics                                                                | `false` |
| `metrics.service.annotations`          | Annotations for Prometheus metrics service                                                             | `{}`    |
| `metrics.serviceMonitor.enabled`       | if `true`, creates a Prometheus Operator ServiceMonitor (also requires `metrics.enabled` to be `true`) | `false` |
| `metrics.serviceMonitor.namespace`     | Namespace in which Prometheus is running                                                               | `nil`   |
| `metrics.serviceMonitor.interval`      | Interval at which metrics should be scraped.                                                           | `nil`   |
| `metrics.serviceMonitor.scrapeTimeout` | Timeout after which the scrape is ended                                                                | `nil`   |
| `metrics.serviceMonitor.selector`      | Prometheus instance selector labels                                                                    | `{}`    |


### Grafana Image Renderer parameters

| Name                                                 | Description                                                                                            | Value                            |
| ---------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | -------------------------------- |
| `imageRenderer.enabled`                              | Enable using a remote rendering service to render PNG images                                           | `false`                          |
| `imageRenderer.image.registry`                       | Grafana Image Renderer image registry                                                                  | `docker.io`                      |
| `imageRenderer.image.repository`                     | Grafana Image Renderer image repository                                                                | `bitnami/grafana-image-renderer` |
| `imageRenderer.image.tag`                            | Grafana Image Renderer image tag (immutable tags are recommended)                                      | `3.0.1-debian-10-r6`             |
| `imageRenderer.image.pullPolicy`                     | Grafana Image Renderer image pull policy                                                               | `IfNotPresent`                   |
| `imageRenderer.image.pullSecrets`                    | Grafana image Renderer pull secrets                                                                    | `[]`                             |
| `imageRenderer.replicaCount`                         | Number of Grafana Image Renderer Pod replicas                                                          | `1`                              |
| `imageRenderer.podAnnotations`                       | Grafana Image Renderer Pod annotations                                                                 | `{}`                             |
| `imageRenderer.nodeSelector`                         | Node labels for pod assignment                                                                         | `{}`                             |
| `imageRenderer.tolerations`                          | Tolerations for pod assignment                                                                         | `[]`                             |
| `imageRenderer.affinity`                             | Affinity for pod assignment                                                                            | `{}`                             |
| `imageRenderer.resources.limits`                     | The resources limits for Grafana Image Renderer containers                                             | `{}`                             |
| `imageRenderer.resources.requests`                   | The requested resources for Grafana Image Renderer containers                                          | `{}`                             |
| `imageRenderer.securityContext.enabled`              | Enable securityContext on for Grafana Image Renderer deployment                                        | `true`                           |
| `imageRenderer.securityContext.fsGroup`              | Group to configure permissions for volumes                                                             | `1001`                           |
| `imageRenderer.securityContext.runAsUser`            | User for the security context                                                                          | `1001`                           |
| `imageRenderer.securityContext.runAsNonRoot`         | Run containers as non-root users                                                                       | `true`                           |
| `imageRenderer.service.port`                         | Grafana Image Renderer metrics port                                                                    | `8080`                           |
| `imageRenderer.metrics.enabled`                      | Enable the export of Prometheus metrics                                                                | `false`                          |
| `imageRenderer.metrics.annotations`                  | Prometheus annotations                                                                                 | `{}`                             |
| `imageRenderer.metrics.serviceMonitor.enabled`       | if `true`, creates a Prometheus Operator ServiceMonitor (also requires `metrics.enabled` to be `true`) | `false`                          |
| `imageRenderer.metrics.serviceMonitor.namespace`     | Namespace in which Prometheus is running                                                               | `nil`                            |
| `imageRenderer.metrics.serviceMonitor.interval`      | Interval at which metrics should be scraped.                                                           | `nil`                            |
| `imageRenderer.metrics.serviceMonitor.scrapeTimeout` | Timeout after which the scrape is ended                                                                | `nil`                            |


